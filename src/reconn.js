var amqp = require("amqplib/callback_api");
const { workerData } = require("worker_threads");

class AMQ {
  constructor() {
    // this.url = "amqp://eosuser:eosuser@localhost?heartbeat=45";
    this.url = "amqp://eosuser:eosuser@1.1.21.238?heartbeat=45";
    this.amqpConn = null;

    this.pubChannel = null;
    this.offlinePubQueue = [];

    this.exchange = "my_test_exchange";

  }

  start = () => {
    amqp.connect(this.url, (error, conn) => {
      if (error) {
        console.error("[AMQP]", err.message);
        return setTimeout(start, 1000);
      }

      conn.on("error", (err) => {
        if (err.message !== "Connection closing") {
          console.error("[AMQP] conn error", err.message);
        }
      });

      conn.on("close", function () {
        console.error("[AMQP] reconnecting");
        return setTimeout(this.start, 1000);
      });

      console.log("[AMQP] connected");
      this.amqpConn = conn;

      this.whenConnected();

    });
  };

  whenConnected = ()=>{
    this.startPublisher();
    this.startWorker();
  }


  startPublisher = ()=> {
      this.amqpConn.createConfirmChannel((err, ch)=>{
        if(this.closeOnErr(err)) return;
        ch.on("error", (err) => {
            console.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            console.log("[AMQP] channel closed");
        })

        this.pubChannel = ch;
        while(true) {
            var m = this.offlinePubQueue.shift();
            if(!m) {break;}
            this.publish(m[0], m[1], m[2]);
        }

      });

  }

  publish = (exchange, routingKey, content) => {
    try {
        this.pubChannel.publish(this.exchange, routingKey, content, {persistent : true}, (err, ok)=>{
            if(err) {
                console.error("[AMQP] publish", err);
                offlinePubQueue.push([exchange, routingKey, content]);
                pubChannel.connection.close();
            }
        });
    } catch(e) {
        console.error("[AMQP] publish", e.message);
        offlinePubQueue.push([exchange, routingKey, content]);
    }
  }


  startWorker = ()=> {

    /*
     * inner function
     * processMsg
     * 
     */ 
    function processMsg(msg) {
      var incomingDate = (new Date()).toISOString();
      console.log("Msg [deliveryTag=" + msg.fields.deliveryTag + "] arrived at " + incomingDate);
      work(msg, (ok)=> {
        console.log("Sending Ack for msg at time " + incomingDate);

        try {
          ch.ack(msg);  
        } catch (e) {
          closeOnErr(e);
        }

      });
    }

    this.amqpConn.createChannel((err, ch) => {
      if(this.closeOnErr(err)) return;
      ch.on("error", (err) => {
        console.err("[AMQP] channel error", err.message);
      });
      ch.on("close", ()=>{
        console.log("[AMQP] channel closed");
      });

      ch.prefetch(10);
      ch.assertQueue("jobs", {durable : true }, (err, _ok) => {
        if(this.closeOnErr(err)) return;
        ch.consume("jobs", processMsg, {noAck : false});
        console.log("Worker is started");
      })
    });


  }

  work = (msg, cb) => {
    console.log("Got msg", msg.content.toString());
    setTimeout(() => cb(true), process.env.WORK_WAIT_TIME || 12000);
  }

  closeOnErr = (err) => {
    if(!err) return false;
    console.error("[AMQP] error", err);
    this.amqpConn.close();
    return true;
  }

}


let amq = new AMQ();
amq.start();