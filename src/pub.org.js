#!/usr/bin/env node

var events = require('events');

// var em = new events.EventEmitter();
// em.on('error', function (data) {
//     console.log('First subscriber: ' + data);
// });

var amqp = require("amqplib/callback_api");
//var url = "amqp://eosuser:eosuser@cbcho";
var url = "amqp://eosuser:eosuser@10.0.22.210?heartbeat=45";
try {
  amqp.connect(url, (error0, connection) => {
    try {
      if (error0) {
        console.log("error : ", error0);
      }

      connection.on("error", function (err) {
        if (err.message !== "Connection closing") {
          console.error("[AMQP] conn error", err.message);
        }
      });
      connection.on("close", function () {
        console.error("[AMQP] reconnecting");
        return setTimeout(start, 7000);
      });

      if (connection) {
        console.log("publisher connected");
      }

      connection.createChannel((error1, channel) => {
        var exchange = "test_exchange_durable_true";
        channel.assertExchange(exchange, "direct", {
          durable: true,
        });

        let counter = 0;

        const interval = setInterval(() => {
          try {
            counter++;
            var msg = `Hello world ${counter}`;
            channel.publish(exchange, "", Buffer.from(msg));
            console.log("data published [", msg, "]");
          } catch (se) {
            console.log(se);
          }
        }, 1000);
      });
    } catch (be) {
      console.log(be);
    }
  });
} catch (bbe) {
  console.log(bbe);
}
