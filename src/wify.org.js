const wifi = require("node-wifi");

const _ssid = "WONIK_ETC";
const _password = "ddtfat@7";
const _targetLevel = -65;
const _iface = "wlp3s0";

// Initialize wifi module
// Absolutely necessary even to set interface to null
wifi.init({
  iface: _iface, // network interface, choose a random wifi interface if set to null
});

// // Scan networks
// wifi.scan((error, networks) => {
//   if (error) {
//     console.log(error);
//   } else {
//     console.log(networks);
//     /*
//         networks = [
//             {
//               ssid: '...',
//               bssid: '...',
//               mac: '...', // equals to bssid (for retrocompatibility)
//               channel: <number>,
//               frequency: <number>, // in MHz
//               signal_level: <number>, // in dB
//               quality: <number>, // same as signal level but in %
//               security: 'WPA WPA2' // format depending on locale for open networks in Windows
//               security_flags: '...' // encryption protocols (format currently depending of the OS)
//               mode: '...' // network mode like Infra (format currently depending of the OS)
//             },
//             ...
//         ];
//         */
//   }
// });

// Connect to a network
wifi.connect({ ssid: "WONIK_ETC", password: "ddtfat@7" }, () => {
  console.log("Connected");
  // on windows, the callback is called even if the connection failed due to netsh limitations
  // if your software may work on windows, you should use `wifi.getCurrentConnections` to check if the connection succeeded
});

const _sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay));

sleepSync = (ms) => {
  const wakeUpTime = Date.now() + ms;
  while (Date.now() < wakeUpTime) {}
};

// Disconnect from a network
// not available on all os for now
// wifi.disconnect(error => {
//   if (error) {
//     console.log(error);
//   } else {
//     console.log('Disconnected');
//   }
// });

// Delete a saved network
// not available on all os for now
// wifi.deleteConnection({ ssid: 'ssid' }, error => {
//   if (error) {
//     console.log(error);
//   } else {
//     console.log('Deleted');
//   }
// });

// timeFunc = () => {
//   let connecting = false;
//   console.log("time function called");
//   wifi.getCurrentConnections((error, currentConnections) => {
//     if (error) {
//       console.error(error);
//     } else {
//       if (currentConnections.length > 0) {
//         const connInfo = currentConnections[0];
//         let sigLevel = connInfo.signal_level;
//         console.log("signal level : ", sigLevel);
//         if (connecting) {
//           console.log("connecting now....");
//           return;
//         }
//         if (sigLevel < _targetLevel) {
//           console.log("trying to reconnect");
//           // console.log("trying to disconnect < ", _targetLevel);
//           wifi.disconnect((error) => {
//             if (error) {
//               console.log(error);
//             } else {
//               console.log("Disconnected from SSID. and try to connect");
//               connecting = connecting = true;
//               wifi.connect({ ssid: _ssid, password: _password }, () => {
//                 console.log("Connected");
//                 // on windows, the callback is called even if the connection failed due to netsh limitations
//                 // if your  software may work on windows, you should use `wifi.getCurrentConnections` to check if the connection succeeded
//                 connecting = false;
//               });
//             }
//           });
//         }
//       }
//     }
//   });
// };
//   console.log("current conn info ");
//   console.log(currentConnections);
/*
      // you may have several connections
      [
          {
              iface: '...', // network interface used for the connection, not available on macOS
              ssid: '...',
              bssid: '...',
              mac: '...', // equals to bssid (for retrocompatibility)
              channel: <number>,
              frequency: <number>, // in MHz
              signal_level: <number>, // in dB
              quality: <number>, // same as signal level but in %
              security: '...' //
              security_flags: '...' // encryption protocols (format currently depending of the OS)
              mode: '...' // network mode like Infra (format currently depending of the OS)
          }
      ]
      */

let connecting = false;
timeFunc = async () => {
  wifi.getCurrentConnections((error, currentConnections) => {
    if (error) {
      console.error(error);
    } else {
      if (currentConnections.length > 0) {
        const connInfo = currentConnections[0];
        let sigLevel = connInfo.signal_level;

        console.log("signal level : ", sigLevel);

        if (sigLevel < _targetLevel) {
          wifi.disconnect((error) => {
            connectiong = true;
            console.log(
              "Disconnected from SSID. and try to connect. wait 5 seconds for recon. set connecting true"
            );
            wifi.connect({ ssid: _ssid, password: _password }, () => {
              connectiong = false;
              console.log("get new connection");
            });
          });
        }
      }
    }
  });
};

// timeFunc = async() => {

//   wifi.disconnect((error) => {
//     if (error) {
//       console.log(error);
//     } else {
//       console.log("Disconnected and wait 5 seconds for recon.....");
//       const s = _sleep(5*1000).then(()=>{
//         wifi.connect(
//         { ssid: "GUEST_WIFI", password: "zxcvb0612" },
//         () => {
//           console.log("Connected");
//         }
//         );

//       });

//     }
//   });
// }

const interval = setInterval(timeFunc, 5 * 1000);

// All functions also return promise if there is no callback given
// wifi
//   .scan()
//   .then(networks => {
//     // networks
//   })
//   .catch(error => {
//     // error
//   });
