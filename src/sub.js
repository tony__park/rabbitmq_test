#!/usr/bin/env node

var amqp = require("amqplib/callback_api");
//var url = "amqp://eosuser:eosuser@cbcho";
var url = "amqp://eosuser:eosuser@10.0.22.210?heartbeat=45";

 connect = () => {
  amqp.connect(url, (error0, connection) => {
    if (error0) {
      console.log("error : ", error0);
    }

    connection.createChannel((error1, channel) => {
      var exchange = "test_exchange_durable_true";
      channel.assertExchange(exchange, "direct", {
        durable: true,
      });

      channel.assertQueue(
        "test_durable_queue",
        {
          durable: true,
        },
        (error2, queue) => {
          if (error2) {
            throw error2;
          }

          console.log(
            " [*] Waiting for messages in %s. To exit press CTRL+C",
            queue.queue
          );
          channel.bindQueue(queue.queue, exchange, "");
          channel.consume(
            queue.queue,
            (msg) => {
              if (msg.content) {
                console.log(" [x] %s", msg.content.toString());
              }
            },
            {
              noAck: true,
            }
          );
        }
      );

      connection.on("error", function (err) {
        if (err.message !== "Connection closing") {
          console.error("[AMQP] conn error", err.message);
        }
      });
      connection.on("close", function () {
        console.error("[AMQP] reconnecting");

        connect();

      });
    });

  });
};

connect();