var amqp = require("amqplib/callback_api");

class Producer {
  constructor() {
    this.amqpConn = null;
    this.url = "amqp://eosuser:eosuser@1.1.20.232?heartbeat=10";

    this.pubChannel = null;
    this.offlinePubQueue = [];

    this.exchange = "my-reconn-exchange";

  }

  start = () => {
    console.log("start");
    
    amqp.connect(this.url, (error, conn) => {
      if (error) {
        console.error("[AMQP]", error.message);
        return setTimeout(this.start, 1000);
      }

      conn.on("error", (err) => {
        if (err.message !== "Connection closing") {
          console.error("[AMQP] conn error", err.message);
        }
      });


      conn.on("close",  ()=> {
        console.error("[AMQP] reconnecting");
        return setTimeout(this.start, 1000);
      });

      console.log("[AMQP] connected");
      this.amqpConn = conn;

      this.whenConnected();
    });
  };

  whenConnected = () => {
      this.startPublisher();
  };

  startPublisher = () => {
    this.amqpConn.createConfirmChannel((err, ch) =>{
      if (this.closeOnErr(err)) return;
      ch.on("error",  (err) => {
        console.error("[AMQP] channel error", err.message);
      });
      ch.on("close", ()  =>  {
        console.log("[AMQP] channel closed");
      });

      ch.assertExchange(this.exchange, "direct", {
        durable: true,
      });


      this.pubChannel = ch;
      while (true) {
        var m = this.offlinePubQueue.shift();
        console.log("M = ", m);
        if (!m) {
            console.log("M not defined. break");
            break;
        }
        this.publish(m[0], m[1], m[2]);
        console.log("published...");
      }
    });
  };

  publish = (exchange, rk /*routingKey*/, content) => {
    try {
      this.pubChannel.publish(
        exchange,
        rk,
        content,
        { persistent: true },
        (err, ok) => {
          if (err) {
            console.error("[AMQP] publish : ", err);
            this.offlinePubQueue.push([exchange, rk, content]);
            // ijpark this.pubChannel.connection.close();
          }
          console.log("published : ", content.toString());
        }
      );
    } catch (e) {
      console.error("[AMQP] publish : ", e.message);
      this.offlinePubQueue.push([exchange, rk, content]);
    }
  };

  closeOnErr = (err) => {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
  };

  timedWork = () => {
    let index = 0;
    let content = "";
    setInterval(()=>{
        content = `hello world ${index}`;
        this.publish(this.exchange, "", new Buffer.from(content));
        // console.log("publish : ", content, ", offline queue : ", this.offlinePubQueue.length);
        index++;
    }, 1000)
  };
}


let p = new Producer();
p.start();
p.timedWork();

