var amqp = require("amqplib/callback_api");

class Worker {
  constructor() {
    this.amqpConn = null;
    this.url = "amqp://eosuser:eosuser@1.1.20.232?heartbeat=10";
    this.exchange = "my-reconn-exchange";
    this.amqCh = null;
  }

  start = () => {
    amqp.connect(this.url, (err, conn) => {
      if (err) {
        console.error("[AMQP]", err.message);
        return setTimeout(this.start, 1000);
      }

      conn.on("error", (err) => {
        if (err.message !== "Connection closing") {
          console.error("[AMQP] conn error", err.message);
        }
      });

      conn.on("close", () => {
        console.error("[AMQP] reconnecting");
        return setTimeout(this.start, 1000);
      });

      console.log("[AMQP] connected");
      this.amqpConn = conn;

      this.whenConnected();
    });
  };

  whenConnected = () => {
    this.startWorker();
  };

  startWorker = () => {
    this.amqpConn.createChannel((err, ch) => {

      if (this.closeOnErr(err)) {
        return;
      }

      ch.on("error", (err) => {
        console.error("[AMQP] channel error", err.message);
      });

      ch.on("close", () => {
        console.log("[AMQP] channel closed");
      });

      ch.assertExchange(this.exchange, "direct", {
        durable: true,
      });

      this.amqCh = ch;

      ch.prefetch(10);
      let qname = "my-queue";
      ch.assertQueue(qname, { durable: true }, (err, queue) => {
        if (this.closeOnErr(err)) {
          return;
        }
        console.log(
            " [*] Waiting for messages in %s. To exit press CTRL+C",
            queue.queue
        );
        ch.bindQueue(queue.queue, this.exchange, "");
        ch.consume( queue.queue, 
                    (msg) => {
                        this.processMsg(msg);
                    }, 
                    { noAck: false }
                    );
        console.log("worker is started");
      });
    });
  };

  processMsg = (msg) => {
    var incomingDate = new Date().toISOString();
    console.log(
      "Msg [deliveryTag=" +
        msg.fields.deliveryTag +
        "] arrived at " +
        incomingDate
    );

    this.work(msg, (ok) => {
      console.log("Sending Ack for msg at time " + incomingDate);
      try {
        if (ok) this.amqCh.ack(msg);
        else ch.reject(msg, true);
      } catch (e) {
        this.closeOnErr(e);
      }
    });
  };

  work = (msg, cb) => {
    console.log("Got msg", msg.content.toString());
    setTimeout(() => cb(true), /*process.env.WORK_WAIT_TIME ||*/ 1);
  };

  closeOnErr = (err) => {
    if (!err) {
      return false;
    }
    console.error("[AMQP] error", err);
    this.amqpConn.close();
    return true;
  };
}

let w = new Worker();
w.start();
