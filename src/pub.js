#!/usr/bin/env node

var events = require("events");

// var em = new events.EventEmitter();
// em.on('error', function (data) {
//     console.log('First subscriber: ' + data);
// });

var amqp = require("amqplib/callback_api");
const { throws } = require("assert");
//var url = "amqp://eosuser:eosuser@cbcho";
var url = "amqp://eosuser:eosuser@10.0.22.210?heartbeat=45";

createConnection = () => {};

class P {
  constructor() {
    this.serverChannel = null;
    this.connError = false;
    this.conn = null;

    this.exchange = "test_exchange_durable_true";

  }

  connectToServer() {
      console.log("start connection")
    amqp.connect(
      url,
      function (err0, conn) {
        if (err0) {
          this.error(`connection failed. ${err0}`);
          return;
        }

        console.log("connected");

        conn.createChannel(
          function (err1, ch) {
            if (err1) {
              throw new Error(`failed to create a channel. ${err1}`);
            }

            this.serverChannel = ch;
            this.connError = false;
            console.log("channel created. and connError False");
          }.bind(this)
        );

        this.conn = conn;

        conn.on("error", function (err) {
          this.connError = true;
          if (err.message !== "Connection closing") {
            console.error("[AMQP] conn error", err.message);
          }
        });

        conn.on("close", function () {
          console.error("[AMQP] closed");
          this.connError = true;
        });

        console.log("connected");
      }.bind(this)
    );
  }

  sleep(ms) {
    const wakeUpTime = Date.now() + ms;
    while (Date.now() < wakeUpTime) {}
  }

  sendMessage = (message) => {
    if (!this.serverChannel) {
      console.log("cannot send a message to ACS. (invalid comm channel)");
      return;
    }
    try {
      var exchange = "test_exchange_durable_true";
      this.serverChannel.assertExchange(exchange, "direct", {
        durable: true,
      });
      this.serverChannel.publish(exchange, "", Buffer.from(message));
      console.log(message);
    } catch (ex) {
      console.log(ex);
      console.log("try reconn");
      this.connectToServer();
      console.log("sleep 5 sec");
      this.sleep(10 * 1000);
      console.log("try fiish");
      var exchange = "test_exchange_durable_true";
      this.serverChannel.assertExchange(exchange, "direct", {
        durable: true,
      });
      this.serverChannel.publish(exchange, "", Buffer.from(message));
      console.log("try sent fin");
      console.log(message);
    }
  }; // end of sendMessage
} // end of class

const p = new P();
p.connectToServer();
let counter = 0;
const interval = setInterval(() => {
  p.sendMessage(`hello world ${counter++}`);
}, 1000);

// try {
//   amqp.connect(url, (error0, connection) => {
//     try {
//       if (error0) {
//         console.log("error : ", error0);
//       }

//       if (connection) {
//         console.log("publisher connected");
//       }

//       connection.createChannel((error1, channel) => {
//         var exchange = "test_exchange_durable_true";
//         channel.assertExchange(exchange, "direct", {
//           durable: true,
//         });

//         let counter = 0;

//         const interval = setInterval(() => {
//           try {
//             counter++;
//             var msg = `Hello world ${counter}`;
//             channel.publish(exchange, "", Buffer.from(msg));
//             console.log("data published [", msg, "]");
//           } catch (se) {
//             console.log("-------------->>>", se);
//           }
//         }, 1000);

//         connection.on("error", function (err) {
//           if (err.message !== "Connection closing") {
//             console.error("[AMQP] conn error", err.message);
//           }
//         });
//         connection.on("close", function () {
//           console.error("[AMQP] reconnecting");
//           // return setTimeout(start, 7000);
//         });
//       });
//     } catch (be) {
//       console.log(be);
//     }
//   });
// } catch (bbe) {
//   console.log(bbe);
// }
